﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using System.Management;
using System.Reflection;
using LibreHardwareMonitor.Hardware;
using System.IO;
using System.Diagnostics;
using System.Net;

namespace RCPC
{
    internal class Program
    {
        static long adminId = 65530966;
        static string telegramAccessToken = "";

        static void Main(string[] args)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            Console.WriteLine("Запуск сервера...");
            var client = new TelegramBotClient(telegramAccessToken);
            client.StartReceiving(Update, Error);
            Console.WriteLine("Сервер запущен.");
            // Считывание команд
            client.SendTextMessageAsync(adminId, "🖥 Сервер запущен.");
            Console.ReadLine();
        }

        async static Task Update(ITelegramBotClient botClient, Update update, CancellationToken token)
        {
            string commandList = "/reboot - перезагрузка сервера.\n/shutdown - выключение сервера.\n/temp - температура CPU.\n/disk_space - свободное место\n/proc_list - список процессов";
            var message = update.Message;
            if (message.Text != null && message.From.Id == adminId)
            {
                Console.WriteLine(message.From.Id + ": " + message.Text);
                // Обработчик команды /start
                if (message.Text.ToLower().Contains("/start"))
                {
                    string backMessage = "Доступ подтвержден.\n";
                    await botClient.SendTextMessageAsync(message.From.Id, backMessage + commandList, cancellationToken: token);
                }

                else if (message.Text.ToLower().Contains("/reboot"))
                {
                    await botClient.SendTextMessageAsync(message.From.Id, "🔁 Перезагрузка сервера...", cancellationToken: token);
                    Process.Start("shutdown.exe", "-r -t -f 0");
                }

                else if (message.Text.ToLower().Contains("/shutdown"))
                {
                    await botClient.SendTextMessageAsync(message.From.Id, "🖥 Выключение сервера...", cancellationToken: token);
                    Process.Start("shutdown.exe", "-s -t 0");
                }

                else if (message.Text.ToLower().Contains("/proc_list"))
                {
                    string procList = "";
                    Process[] procs = Process.GetProcesses();

                    foreach (Process p in procs)
                    {
                        if (p.MainWindowTitle != "" && p.MainWindowTitle != " ")
                        {
                            procList += "🖥 " + p.MainWindowTitle + "\n";
                            Console.WriteLine(p.MainWindowTitle);
                        }
                        else if (p.ProcessName != "svchost")
                        {
                            procList += p.ProcessName + "\n";
                            Console.WriteLine(p.MainWindowTitle);
                        }
                    }
                    await botClient.SendTextMessageAsync(message.From.Id, "🖥 Список процессов:\n" + procList, cancellationToken: token);
                }

                else if (message.Text.ToLower().Contains("/disk_space"))
                {
                    double free = 0;
                    double a = 0;
                    string Vol = "";
                    DriveInfo[] allDrives = DriveInfo.GetDrives();
                    foreach (DriveInfo MyDriveInfo in allDrives)
                    {
                        if (MyDriveInfo.IsReady == true)
                        {
                            if (MyDriveInfo.Name != "D:\\") 
                            {
                                free = MyDriveInfo.AvailableFreeSpace;
                                a = (free / 1024) / 1024 / 1024;
                                Vol += "💾 " + MyDriveInfo.Name + " - " + a.ToString("#.##") + " Gb." + Environment.NewLine;
                            }
                        }
                    }
                    await botClient.SendTextMessageAsync(message.From.Id, "🗄 Свободное место на дисках:\n" + Vol, cancellationToken: token);
                }

                else if (message.Text.ToLower().Contains("/temp"))
                {
                    await botClient.SendTextMessageAsync(message.From.Id, "Ожидайте...", cancellationToken: token);
                    Computer computer = new Computer
                    {
                        IsCpuEnabled = true,
                        IsGpuEnabled = true,
                        IsMemoryEnabled = true,
                        IsMotherboardEnabled = true,
                        IsControllerEnabled = true,
                        IsNetworkEnabled = true,
                        IsStorageEnabled = true
                    };

                    computer.Open();
                    computer.Accept(new UpdateVisitor());

                    string cpuTempData = "";

                    foreach (IHardware hardware in computer.Hardware)
                    {
                        if (hardware.HardwareType == HardwareType.Cpu)
                        {
                            Console.WriteLine("Устройство: " + hardware.Name);
                            cpuTempData += "🖥 " + hardware.Name + "\n";
                            foreach (ISensor sensor in hardware.Sensors)
                            {
                                if (sensor.SensorType == SensorType.Temperature)
                                {
                                    Console.WriteLine("\tСенсор: {0}, value: {1}", sensor.Name, sensor.Value);
                                    cpuTempData += "🌡 " + sensor.Name + ": " + sensor.Value + "°C\n";
                                }
                            }
                        }
                        else if (hardware.HardwareType == HardwareType.GpuNvidia)
                        {
                            Console.WriteLine("Устройство: " + hardware.Name);
                            cpuTempData += "🖥 " + hardware.Name + "\n";
                            foreach (ISensor sensor in hardware.Sensors)
                            {
                                if (sensor.SensorType == SensorType.Temperature)
                                {
                                    Console.WriteLine("\tСенсор: {0}, value: {1}", sensor.Name, sensor.Value);
                                    cpuTempData += "🌡 " + sensor.Name + ": " + sensor.Value + "°C\n";
                                }
                            }
                        }
                    }
                    await botClient.SendTextMessageAsync(message.From.Id, "🌡 Температура:\n\n" + cpuTempData, cancellationToken: token);
                }
            } 
        }

        async static Task Error(ITelegramBotClient botClient, Exception ex, CancellationToken token)
        {
            var message = ex.Message;
            Console.WriteLine(message);
        }

        public class UpdateVisitor : IVisitor
        {
            public void VisitComputer(IComputer computer)
            {
                computer.Traverse(this);
            }
            public void VisitHardware(IHardware hardware)
            {
                hardware.Update();
                foreach (IHardware subHardware in hardware.SubHardware) subHardware.Accept(this);
            }
            public void VisitSensor(ISensor sensor) { }
            public void VisitParameter(IParameter parameter) { }
        }
    }
}
